WHERE'S THE CODE?
-----------------

You've just checked out the code from Git's "master" branch. In Drupal module
development, the code lives in branches with names that mirror the released
version numbers.

For example:
The code for a project's 7.x-1.x-dev release is found in the 7.x-1.x Git branch.

To see a list of all branches for this project using Git on the command line,
use the following command:

  git branch -a

To switch from master branch to the 7.x-1.x branch using Git on the command
line, use the following command:

  git checkout 7.x-1.x
